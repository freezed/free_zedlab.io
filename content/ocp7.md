Title: GrandPy Bot, le papy-robot
Date: 2018-09-17 11:11
Category: Réalisations
Status: published
Summary: Un chat bot web dans un contexte géographique
Tags: flask, api-rest, heroku, tdd, git, python, dev,


Dans une interface web, il est proposé de dialoguer dans un contexte géographique avec avec un _Grand Père virtuel_.

Vous le questionnez sur un lieu et il répond avec une carte et un texte en provenance de Wikipedia.

* Interactions en AJAX : réponse s'affiche sans recharger la page
* API [Google Maps][gmaps] et [Media Wiki][mediawiki]
* Pas de sauvegarde (page rechargée == historique perdu)
* Interface responsive
* Test Driven Development (mocks pour les API)
* Déploiement avec [Heroku][heroku]

---

Voir le code hébergé sur [Github][ocp7]


[gmaps]: https://cloud.google.com/maps-platform/?hl=fr "API Google Maps"
[heroku]: https://devcenter.heroku.com/articles/getting-started-with-python
[mediawiki]: https://www.mediawiki.org/wiki/API:Main_page/fr
[ocp7]: https://github.com/freezed/ocp7/tree/v0.2
