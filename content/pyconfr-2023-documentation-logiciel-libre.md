Title: Documentation, logiciel libre et pérennité en arts numériques
Date: 2023-02-19 11:01
Summary: SUMMARY
Category: Bloc-notes
Tags: live-notes, pyconfr, talk, bordeaux, python, documentation, logiciel-libre
Status: published

Par **[Édith Viau][author]** - Salle [Thomas Edison][tedison]


### [Documentation, logiciel libre et pérennité en arts numériques][abstract]

[![logo PyConFr Bordeaux 2023][pyconimg]][pyconfr]

> Les arts numériques posent des défis de conservation et de préservation importants, de par leurs contraintes matérielles et logicielles.
>
> Une utilisation judicieuse de composantes sous licence libre peut favoriser la pérennité des oeuvres réalisées avec les technologies modernes.
>
> Une documentation technique adéquate constitue un facteur de succès important pour un tel projet. Voici quelques réflexions sur le sujet ainsi que des lignes directrices pour vous guider, basés sur notre expérience avec Sphinx, le logiciel utilisé pour la documentation de Python, au Metalab, le laboratoire de recherche et développement de la Société des arts technologiques (Montréal, Québec, Canada).

_[Support][support]_

---

Notes personnelles
==================

* presentation
    - contributrice traduction python
    - responsable documentation au [Metalab de la Société des Arts Technologique (SAT)](https://sat.qc.ca/en/metalab)
* Comment commencer une doc
    - pratique: on part de la matière et
    - technique
    - pour les AN
    - il faut choisir (tentative d'épuisement d'un lieu pParisien G Perec 1975)
*  Qu'est ce qui est important?
    - documentation vs. interface: machine Antycithère
    - essentiel: le pourquoi et le quoi
    - conseils doc utile sans être complète
        * écrire pour soi: ce que j'aurai aimé quand j'ai commencé
            - `sphinx-quickstart`
        * documenter celon le cas d'usage
            - guide de refence ou tuto?
            - Hello world ou exemple complexe?
            - utilisateur débutant ou confirmé
            - spécificité des AN: des artistes en résidences
        * éditer pour les autres
            - évaluer rapidement l'outil
            - comment il s'imbrique avec les outils existant'
            - tuto et démo pour artistes
* Pourquoi documenter lorsque l'on fait le dev?
    - améliorer l'existant
    - clarifier ses idées
* On documente quoi?
    - la carte ou le territoire?
    - ennoncé les choix arbitraires des choix motivés
    - garder le lien avec les communautés
* Exemple #1 [`splash`](http://sat-metalab.gitlab.io/splash/doxygen/index.html)
    - minimiser les prérequis: avoir un objet modélisable disponible partout?
        * cube: objet simple mais peu de personne en ont a dspo
        * un coin de mur! tout le monde en a un et on peut joindre un modèle 3D dans la doc que tout le monde pourra utiliser
        * construire sur les acquis:  on commence avec un projecteur, plus de projecteurs dans des étapes suivantes
* REX
* Exemple #2 [`LivePose`](https://gitlab.com/sat-mtl/tools/livepose): ML pour AN
    - besoin de communiquer avec d'autres outils
    - rédiger un liens JS
* **Conclusion:** comment faire une doc utile sans être complète
    - ce que l'on fait
    - pourquoi on le fait'
    - ce qui n'est pas dans le code (carte vs. territoire)
* Références
    - Blender docs: pair documenting
    - Diàtaxis: architecture de la documentation
    - [How to make sense of any mess](https://www.youtube.com/watch?v=s6lXWgnTmRE): Quand on a beaucoup de doc à (re)structurer…
    - Write the Docs
    - Rédaction technique en information - UdeSherbrooke
    - Rafael Lozano-Hemmer: Best practices for conservation of media arts texte - (vidéo est à voir)
    - Doc Sphinx
* Questinos:
    - intéret de git: oui mais peut-être plus encore de la CI
    - test s'assurant que l'outil n'a pas été mis a jour sans la doc: non, mais c'est un sujet en discussion dans la communauté Read the docs
    - mesurer si les utilisateurs ont été perdu ou gagné? Collecte de retour utilisateurs multi-canaux


[abstract]: https://www.pycon.fr/2023/fr/talks/1h.html#documentation-logiciel-libre-e
[author]: https://codame.com/artists/edith-viau
[hpoincare]: https://fr.wikipedia.org/wiki/Henri_Poincaré
[pyconfr]: https://www.pycon.fr/2023/
[pyconimg]: {static}/img/200-pycon-fr-23.png
[support]: https://
