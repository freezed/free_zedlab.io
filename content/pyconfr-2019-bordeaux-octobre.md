Title: Vider sa flask dans une lambda
Date: 2019-11-03 12:00
Category: Conférences
Summary: Déployer une application Flask sur AWS Lambda avec Zappa & GitLab
Status: published
Tags: pyconfr, talk, git, gitlab, devops, flask, dev, zappa, serverless, aws, aws-lambda, bordeaux, python

[20/7/20] : J'ajoute le lien vers la captation vidéo dispo sur [pyvideo.org](https://pyvideo.org/pycon-fr-2019/deployer-flask-sur-aws-lambda-avec-zappa-gitlab.html)

---

[![logo PyConFr Bordeaux 2019][pyconimg]][support]
Présentation d'un [chatbot basique][repo] utilisant une application mono-page Flask (avec une larme de JS) qui répond avec des données provenant des API MapQuest et Wikipedia.

La particularité offerte par Zappa est d'avoir un Flask _classique_ en local (pour le dev) et de simplifier le déploiement vers AWS-Lambda pour la mise à dispo vers le monde. Gitlab permet d'automatiser l'usage de Zappa, mettre à jour le JS avec la réponse AWS, puis héberger les fichiers statiques .

Cette [présentation publique][conference] s'est déroulée dans le cadre de la
 [Conférence Python Francophone][pyconfr] (31/10 - 3/11/2019) organisée par [l'AFPy][afpy]. Cet évènement majeur est dédié au regroupement des personnes intéressées par le langage de programmation [Python][py].

---

Mon autre _bac à sable Zappa_ : [_Hello Zappa_][hz]

Le support est disponible en cliquant **sur le logo** de _PyConFr_ ci-dessus.

[afpy]: https://afpy.org
[conference]: https://www.pycon.fr/2019/fr/talks/conference.html#d%C3%A9ployer%20flask%20sur%20aws%20lambda%20avec%20zappa%20%26%20gitlab
[hz]: https://gitlab.com/free_zed/hellozappa/
[pyconfr]: https://www.pycon.fr/2019/fr/
[pyconimg]: {static}/img/250-pycon-fr19.png
[py]: https://python.org
[repo]: https://gitlab.com/free_zed/grandpy
[support]: https://gitpitch.com/free_zed/grandpy/pyconfr?grs=gitlab
