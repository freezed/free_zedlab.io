Category: Bloc-notes
Date: 2023-02-02 14:58
Status: published
Summary: Cette session de live coding sera l'occasion de rentrer dans le détail de ces deux techniques en implémentant les deux techniques.
Tags: vtt, ovh, talk, paris, security, live-coding, admin, logs
Title: DLL Side Loading & Process Injection : comment ça marche ?

Par [Sebastien Meriot][author] - [**DEMO** _Very Tech Trip_][vtt]


### DLL Side Loading & Process Injection : comment ça marche ?

> Les logiciels malveillants modernes utilisent aujourd'hui quasiment tous ces deux techniques. La première vise à changer le comportement d'un exécutable en venant modifier une DLL chargée dynamiquement. Cette DLL viendra alors procéder à l'altération en mémoire d'un processus en cours d'exécution pour changer son comportement et le faire exécuter du code malveillant. C'est ainsi un excellent moyen d'évader les antivirus qui ne se doutent pas qu'un processus légitime, signé par un éditeur de confiance, puisse exécuter du code malveillant.
>
> Cette session de live coding sera l'occasion de rentrer dans le détail de ces deux techniques en implémentant les deux techniques. L'antivirus nous détectera-t-il ?

---

Notes personnelles
==================

* Échapper a la détection des antivirus
* Injection de process?
    - voir `MITRE|ATT&CK
    - utiliser un process de confiance: `explorer.exe` sous windows, un antivirus, etc.
* Example: calculatrice windows
    - win achi: loader / runing process
    - side loading: 2 soft de confiance sont utilisé pour créer un thread
* demo
* Contre messure (windows): config `sysmon` pour le SIEM
    - SwiftOnSecurity Config
    - TrustedSec Guide


[author]: https://www.linkedin.com/in/%F0%9F%94%91-s%C3%A9bastien-m%C3%A9riot-8b1b74a
[vtt]: https://verytechtrip.ovhcloud.com/fr/very-tech-trip/sessions/
