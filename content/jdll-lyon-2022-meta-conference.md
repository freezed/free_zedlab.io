Title: JdLL - La Meta conference
Date: 2022-04-02 16:00
Summary: Mais au fait, d'où ça sort les JdLL ? Qui les organise ? Comment ?
Category: Bloc-notes
Tags: talk, jdll, lyon, collectif, méthode
Status: Published
Lang: fr

### [JdLL - La Meta conference](https://pretalx.jdll.org/jdll2022/talk/EZLCWX/)

par [Stéphane Parunakian](https://stephane.parunakian.fr/) & [Grégoire Cutzach](https://hexaltation.org/) - 2022-04-02 16:00–16:55 Atelier du mouvement

![logo JDLL 2022]({static}/img/jdll-2022.jpg)

Mais au fait, d'où ça sort les JdLL ? Qui les organise ? Comment ?

Les Journées du Logiciel Libre, ça existe depuis 1998 et elles ont bien évoluées depuis !

[Lien vers la captation vidéo][captation]

---

Notes personnelles:

- 1997 -> 1999 mais rien de formel
- 2012 a la MPT - Salle des Rancy
- Organisé par: ALDIL / MPT / Illyse / bénévoles
- Plusieurs rôle, pas fixe d'années en années
- Budget: papier, alimentation bénévoles, matériel,
- Difficulté: équipe changeante et personnel non technique
- Outil état des lieux 2015
    * Liste de diffusion interne
    * Formulaire sur site jdll.org
    * Et c'est tout
- Mise en place outil
    * mattermost
    * redmine -> échec
    * Rapla -> bien mais pas super adapté aux jdll
    * etherpad -> en prod mais a repasser
    * pretalx
    * nextcloud
    * site: grav
    * git: démarrage cette annee
    * peertube
- Avenir
    * outil, mise a jours, etc.
    * Petites nouveauté: concert, projection
- fonte des bénévoles


_[MàJ du 3/10/22]:_ [captation]: https://www.videos-libr.es/w/3gt9rF4NsWdiwkCeeQ6rwE
