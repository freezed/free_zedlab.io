Title: Using QEMU as KVM
Date: 2021-09-14 22:59
Summary: Using KVM to run a Debian Live on SYS dedicated server
Category: Bloc-notes
Tags: debian, ovh, admin, shell, cli, vm, qemu
Status: Published
Slug: using-qemu-as-kvm

[_So You Start_ dedicated servers](https://www.soyoustart.com/fr/serveurs-essential/) did not provide [free KVM](https://www.soyoustart.com/fr/kvm-ip.xml) to run a random OS iso.
Let's try with [`QEMU`](https://github.com/qemu/qemu#readme) running an iso uploaded locally.

In _rescue mode_, download a live iso locally, install and run `QEMU`:

```bash
apt update && apt upgrade
apt install qemu-system-x86 vim
wget --no-check-certificate -O /tmp/installer.iso https://cdimage.debian.org/mirror/cdimage/release/current-live/amd64/iso-hybrid/debian-live-11.1.0-amd64-standard.iso
qemu-system-x86_64 -net nic -net user,hostfwd=tcp::80-:80,hostfwd=tcp::443-:443 -m 4096M -localtime -enable-kvm -hda /dev/sda  -vnc 0.0.0.0:0 /tmp/installer.iso -boot d
```

---

_Refs & sources_  :

- [_How to create Virtual KVM on Kimsufi / OVH / SoYouStart_](https://rinaldojonathan.com/en/cara-buat-virtual-kvm-di-server-ovh-kimsufi-soyoustart/) by [_Rinaldo Jonathan_](https://rinaldojonathan.com/en/author/admin/) (30/5/2019)
- [_Booting a Debian Live ISO on QEMU_](https://serverfault.com/q/1078487/369964), my question on _serverfault.com_ (23/9/2021)
- [_QEMU full virtualization – CPU emulations (enable/disable CPU flags/instruction sets)_](https://ahelpme.com/howto/qemu-full-virtualization-cpu-emulations-enable-disable-cpu-flags-instruction-sets/) by [_neoX_](https://ahelpme.com/author/admincho/) (3/4/2018)
- [_How to SSH from host to guest using QEMU?_](https://unix.stackexchange.com/q/124681/280824) by [_jobin_](https://unix.stackexchange.com/users/29330/jobin) on _unix.stackexchanqe.com_ (14/4/2014)
- [_KVM: Which CPU features make VMs run better?_](https://serverfault.com/q/404195/369964) by [_PythonLearner_](https://serverfault.com/users/90762/pythonlearner) on _serverfault.com_ (2/7/12)
