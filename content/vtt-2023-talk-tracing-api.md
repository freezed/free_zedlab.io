Title: Trouve les bottlenecks de ton application grâce au tracing !
Date: 2023-02-02 13:30
Category: Bloc-notes
Tags: vtt, ovh, talk, paris, python, performance, dev, devops, observability
Status: published

Par [Victor Coutellier][author] - [**TALK** _Very Tech Trip_][vtt]

_**Support**: sur [`github.com/alistarle/vtt-tracing`][support]_

### Trouve les bottlenecks de ton application grâce au tracing !

> La dimension mondiale d'OVHcloud implique de surveiller la performance de nos APIs au même titre que leur fonctionnement. De nombreux outils existent aujourd'hui pour intégrer simplement du tracing dans votre application quel que soit son langage et ainsi identifier vos potentiels bottleneck avant même le passage en production. Nous vous invitons à découvrir comment, au sein de l'équipe Public Cloud, nous avons mis en place une de ces solutions afin de réduire considérablement le temps de génération des token d'authentification.
>
> Au programme:
> * Ajoutez OpenTracing dans ton application pour générer des traces utiles en un minimum de code
> * Déployez Grafana Tempo pour stocker et visualiser tes traces
> * Conseils pour identifier les usual suspect

---

Notes personnelles
==================

* Openstack keystone / IAM OVH
    - 700 ms pour avoir un token
* Troubleshooting
    - on ne veu ps changer le code pour ajouter du tracing
    - => autoinstrumentation
* [`opentelemetry-instrument`][openti] dispo natif pour
    - `.net`, `python`, `java`, `node`
* Python use case
    - usage des `hook` des dépendances
* Architecture OpenTelemetry
    - Keystone / Grafana + tempo / OTEL operator / OTEL collector / Instrumentation Open telemetry
    - => demo
* Possible sur tout infra


[author]: https://www.linkedin.com/in/victor-coutellier
[vtt]: https://verytechtrip.ovhcloud.com/fr/very-tech-trip/sessions/
[openti]: https://opentelemetry.io
[support]: https://github.com/alistarle/vtt-tracing
