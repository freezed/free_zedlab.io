Category: Bloc-notes
Date: 2023-02-02 16:00
Status: published
Summary: Venez assister à la rencontre de la créativité et de la tech présentée par deux personnages de la série Netflix Arcane, Viktor et Heimerdinger, en Cosplay !
Tags: vtt, ovh, talk, paris, cosplay, iot, diy, live-coding, observability
Title: Cosplay et Tech : la grande aventure 🚀



ar [Thierry _Viktor_ Chantier][author-t] & [Pierre _Heimerdinger_ Tibulle][author-p] - [**TALK** _Very Tech Trip_][vtt]


### Cosplay et Tech : la grande aventure 🚀

> "Viktor continue ses recherches sur la magie HexTech pour trouver une thérapie. De son côté Heimerdinger prend conscience des risques et cherche à contrôler toute cette puissance !"
>
>  Venez assister à la rencontre de la créativité et de la tech présentée par deux personnages de la série Netflix Arcane, Viktor et Heimerdinger, en Cosplay !
>
>  Le cosplay associe des compétences artistiques, artisanales et techniques lors de la création des costumes, mais il prend une toute autre dimension en lui greffant de l'électronique et toute une infrastructure informatique !
>
>  Après une petite introduction au Cosplay, nous vous détaillerons la construction des costumes et accessoires présentés. Vous serez également acteurs de ce talk via diverses interactions rendues possibles par des outils que vous avez l'habitude de mettre en place au quotidien. Venez apprendre en vous amusant !

---

Notes personnelles
==================

* Cosplay?
    - Costume - Play
    - Faire sois même et/ou acheter
* Pierre:
    - c'est de l'émotion, fabrique tout sois même
    - [Heimerdinger](https://leagueoflegends.fandom.com/wiki/Heimerdinger/Arcane): LOL / Arcane
    - fabrique sois même: couture
    - Scan de visage
    - Mousse de matelas pour volume
    - teinture de fourure
* Thierry: _Et si on mettait de la tech?_
    - des oreilles qui bougent
    - Une Arcane box en impression 3D
        * software: mosquitto (MQTT), python, LDP, …
* demo
* Les évolutions
    - Dataviz (look steampunk)
    - monitorer physiquement
    - connecter le masque
    - faire interagir le public avec le masque

[author-t]: https://www.linkedin.com/in/thierrychantier
[author-p]: https://www.linkedin.com/in/pierre-tibulle-68674812b
[vtt]: https://verytechtrip.ovhcloud.com/fr/very-tech-trip/sessions/
