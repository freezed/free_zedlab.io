Title: Faire du Python professionnel
Date: 2023-02-18 10:57
Summary: Arrêtez de bidouiller!
Category: Bloc-notes
Tags: live-notes, pyconfr, talk, bordeaux, python, dev, méthode,
Status: published

Par **[Éric Dasse][author-eric]** & **[Dimitri Merejkowsky][author-dimitri]** - Salle [Charles Darwin][cdarwin]

### [Faire du Python professionnel][abstract]

[![logo PyConFr Bordeaux 2023][pyconimg]][pyconfr]

> Python a la réputation d'être un langage de programmation avec une grande simplicité syntaxique. L'avantage, c'est qu'il est facile à apprendre et donc à mettre en place dans un projet même avec relativement peu d'expérience. Il donne la possibilité d'écrire du code presque exactement comme il nous apparait à l'esprit.
>
> Toutefois, cette même simplicité peut aussi jouer en la défaveur d'un projet sur le long terme si certaines bonnes pratiques ne sont pas mises en place, soit parce qu'on n'est pas informé de ces bonnes pratiques, soit parce qu'on pense gagner du temps en les ignorant.
>
> Dans cette présentation, on vous propose de découvrir comment donner un ton plus professionnel à votre code Python afin de construire vos projets sur de bonnes bases.

_[Support][support]_

---

Notes personnelles
==================

- présentation
    * [arolla - software gardening](https://www.arolla.fr/)
- Python craft
    * syntaxe simple
    * beaucoup de liberté et ça peut **beaucoup** dégénérer
    * je peu faire beaucoup de très mauvaise qualité
    * mais aussi de très bonne qualité
- Bidouiller c'est quoi?
    * juste marche
    * sans considérere le futur, meilleure approche
    * du script
- le danger:
    * maintenance complexe
    * empoisonne la collaboration
    * dette technique
    * cercle vicieux, théorie des tas/vitre cassé
- pourquoi le _clean-code_?
    * intention plus claire
    * pour ajouter plus facilement du code
    * éviter la peur de son propre code
    * debug
- [Zen of Python - PEP 02][pep20]
    * usage des `fstring`
    * remplacer `get` qui est trop répendu
    * usage des clés explicites, sans relire le `dict()`
- [PEP 8][pep8]
- Conventions
    * `_prefix` pour indiqué le caractère privé
    * `snake_case_for_variable_and_function`, `CapitalizedCaseForClasses`, `CONSTANTE_CAPITALIZED`
    * refleter le métier dans les noms
    * éviter les abbreviations
- recommandations
    * ne pas comparer les booleens `if valeur` au lien de `if valeur == True`
    * `is not` au lieu de `not … is`
    * préférer le `startswith` & `endswith` au lieu du _slicing_
    * préférer les _comprehension lists_ (mais pas trop non plus)
    * utiliser le multiparadigme
- la POO
    * **Classe :** Cas d'usage pertinent ou pas
    * **Encapsulation :** protection de variable (càd non modifiable par accident)
    * Les fonctions sont des objets et manipulable comme telles
    * Python aime les _design pattern_
    * Des fonctionnalités attendue
- outils:
    * `black`, `flake8`, `mypy`
    * `flake8` flake8-comprehention
    * `black` a été testé avec différents paramètres pour choisir les paramètres par défaut
    * `mypy`: _exemple dans les slides_, n'intervient pas sur le _runmtime_
        - _static python_ = `python` + `mypy` en mode strict
            * ça devient un autre language
            * vérifier si ça vaut le coup
            * demande des concepts avancé (covariance, contravariance, dependant types, …)
        - -> on trouve des bug, du code à améliorer, refacto moins risqué
        - -> évitez la complexité, les annotations sans les _lint_
        - -> les bons arguments contres: https://dev.to/etenil/why-i-stay-away-from-python-type-annotations-2041
- pas de démo
- conclusion
    * vous avez le choix avec python
    * on peut allez très loin en restant sur python à condition d'ajouter de la rigueur et de l'outillage
    * on a pas parlé des test, de SOLID, _clean-code_, …


[abstract]: https://www.pycon.fr/2023/fr/talks/
[author-dimitri]: https://dmerej.info/blog/pages/about/
[author-eric]: https://fr.linkedin.com/in/eric-dasse-b68a56119
[cdarwin]: https://fr.wikipedia.org/wiki/Charles_Darwin
[pyconfr]: https://www.pycon.fr/2023/
[pyconimg]: {static}/img/200-pycon-fr-23.png
[support]: https://
[pep20]: https://peps.python.org/pep-0020/
[pep8]: https://peps.python.org/pep-0008/
