Title: Modèles économiques du Logiciel Libre et l'association PLOSS RA
Date: 2022-04-03 15:00
Summary: SUMMARY
Category: Bloc-notes
Tags: talk, jdll, lyon, collectif, logiciel libre
Status: Published
Lang: fr

### [Présentation des modèles économiques du Logiciel Libre et de l'association PLOSS RA](https://pretalx.jdll.org/jdll2022/talk/DGFJXP/)

par [Clément Oudot](https://fr.linkedin.com/in/clementoudot) - [Patrick Abiven](https://fr.linkedin.com/in/patrickabiven) - 2022-04-03 15:00–15:55, Vie citoyenne

![logo JDLL 2022]({static}/img/jdll-2022.jpg)

PLOSS RA regroupe des entreprises locales participant à l'écosystème du Logiciel Libre.

Le logiciel libre est aujourd'hui utilisé dans des contextes personnels, associatifs, éducatifs et professionnels. De nombreuses sociétés proposent aujourd'hui des services autour du libre et contribuent à cet écosystème. Il existe de nombreux modèles économiques permettant à ces entreprises de se développer, nous les présenterons en fournissant quelques exemples d'entreprises locales membres de [PLOSS RA](https://www.ploss-ra.fr/).

[Lien vers la captation vidéo][captation]

---

Notes personnelles:

- PLOSS RAA
    - Adhérant au [CNLL](https://cnll.fr/)
    - ~35 entreprise du numérique
    - action: Campus du lbre, FLOSScon, etc.
    - action collectives:
    - force de proposition
- [RPLL 2022](https://www.rpll.fr/-2022-)
    - 28 juin 2022 a l'hotel de région
- LL marché en forte croissance
- LL dans le marché IT 2017
    - 9.9% FR (2020: 11.1%)
    - 6.4% D
    - 6.5% GB
- emploi FR
    - 45000 - 2017
    - 56000 - 2020
- Naissance de l'économie du libre
    - sans rareté pas d'économie
    - Le LL n'est pas rare, c'est le service associé qui est vendu
- Les acteurs ont des métiers différents
- communautaire / entreprise
    1. OS/libre mais sans support
    1. définition variable
- [Worteks](https://www.worteks.com/)
    - expertise
    - 200+ clients
    - 5 an ancienneté
    - 50 / 50 public/privé
    - 70% france
    - 85% europe
    - red hat / [bluemind](https://www.bluemind.net/) / [onlyoffice](https://www.onlyoffice.com/fr/)
- [Apitech](https://apitech.net/)
    - 80% d'activité en LL
    - 1991 / 22 personnes


_[MàJ du 3/10/22]:_ [captation]: https://www.videos-libr.es/w/98huApkGUVavSsFBr5cSyY
