Title: Rejoignez le Fediverse, ajoutez ActivityPub à votre site !
Date: 2023-02-18 12:08
Summary: SUMMARY
Category: Bloc-notes
Tags: live-notes, pyconfr, talk, bordeaux, python, activitypub, dev, web, innovation, privacy
Status: published

Par **[Jérôme Tanghe][author]** − Salle [Rosalind Franklin][rfranklin]


### [Rejoignez le Fediverse, ajoutez ActivityPub à votre site !][abstract]

[![logo PyConFr Bordeaux 2023][pyconimg]][pyconfr]

> En 2022, Elon Musk rachète Twitter. S'ensuit une série de décisions désastreuses amenant de nombreux internautes à se réfugier sur Mastodon, un réseau social qui revendique ne pas être à vendre, et qui a la particularité d'être distribué : le logiciel, publié sous licence libre, est installé sur des serveurs administrés par des volontaires, capables de communiquer entre eux grâce à un protocole nommé ActivityPub
>
> Ce protocole est par ailleurs également utilisé par de plus en plus nombreux logiciels, comme Pixelfed (partage de photos), PeerTube (vidéos) ou Writely (blogs), ce qui les rend capables de communiquer entre eux, créant le "Fediverse".
>
> Durant cette conférence, je vous expliquerai comment fonctionne ce protocole et comment l'implémenter sur votre propre site.

---

Notes personnelles
==================

* Présentation
* Évolution de l'usage de mastodon
* Fediverse
* ActivityPub
    - basé sur REST    p JSON-LD
    - communication inter-serveurs
    - peut faire exploser le cerveau
    - n'est pas reservé aux réseaux sociaux'
* Brancher le blg.afpy.org au Fediverse
    - `@actualites@afpy.org` =>  `@<USER>@<SERVER>`
    - acteur du contenu: `<USER>`
    - _webfinger_: permet la découvrabilité
    - _outbox_: un boite au lettre extérieur, visible publiquement
* Flux ActivityPub mis à jour avec un flux RSS
* pour les sites statique aussi
* liens complémentaires
    - Spécification officielle : https://www.w3.org/TR/activitypub/ (très lisible, profitez-en !)
    - La documentation de Mastodon pour signer ses messages : https://docs.joinmastodon.org/spec/security/
    - Pour suivre les statistiques du Fediverse : https://fediverse.observer
    - Le projet f2ap : https://github.com/Deuchnord/f2ap
    - [Micropub](https://github.com/capjamesg/micropub)


[abstract]: https://www.pycon.fr/2023/fr/talks/30m.html#rejoignez-le-fediverse-ajoutez
[author]: https://deuchnord.fr/
[pyconfr]: https://www.pycon.fr/2023/
[pyconimg]: {static}/img/200-pycon-fr-23.png
[rfranklin]: https://fr.wikipedia.org/wiki/Rosalind_Franklin
