Title: Histoires d'un sportif perfectionniste sous pression [JDLL]
Category: Conférences
Date: 2022-04-03 14:00
Lang: fr
Status: Published
Summary: Garder le contrôle sur les données que je partage avec des services en ligne.
Tags: jdll, talk, lyon, dev, cli, python, privacy


### Garder le contrôle sur les données que je partage avec des services en ligne.

Les 2 & 3 avril 2022 se sont déroulées les [Journées du Logiciel Libre][jdll] à Lyon.

[![logo JDLL 2022]({static}/img/jdll-2022.jpg)](https://pretalx.jdll.org/jdll2022/talk/MKNM8H/)


> _Strava_, _runkeeper_, _runtastic_, … sont des réseaux sociaux pour les activités sportives basé sur l'usage de grandes quantité de données géolocalisés pour fournir en retour divers services.

> Le fonctionnement par défaut ne permet pas facilement de limiter les données transmises et le coût d'usage en données personnel devient très vite élevé si on n'utilise pas la totalité des services proposé.

> J'ai souhaité construire un outil qui permettrait de choisir les données que je transmet, me permettant de bénéficier des quelques services de mon choix en limitant les données utilisé par les services qui ne m'intéressent pas.

> C'est un projet expérimental et ouvert qui permet de mettre en lumière l’économie de la donnée des grands services en ligne.

- Support de présentation : [📦️ archive]({static}/pdf/20220403-jdll-geostrapy.pdf) / [📝 `fcode/geostrapy`][support-froggit]
- Dépôt de code: [`fcode/geostrapy`][repo]

_[MàJ du 3/10/22]:_ [Lien vers la captation vidéo][captation]


[jdll]: https://jdll.org
[repo]: https://lab.frogg.it/fcode/geostrapy
[support-froggit]: https://lab.frogg.it/fcode/geostrapy/-/blob/13-jdll-talk/docs/20220403-jdll-geostrapy.pdf
[captation]: https://www.videos-libr.es/w/wEsek34z3YQpExgs2HLHLH
