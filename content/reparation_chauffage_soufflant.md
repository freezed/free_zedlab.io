Title: Réparation d'un sèche serviette soufflant
Date: 2019-11-18 00:44
Modified: 2021-11-28 02:00
Summary: Réparation d'un sèche serviette soufflant
Category: Bloc-notes
Tags: DIY, réparation, électroménager
Status: published

Si jamais quelqu'un à le même appareil en panne

- ![photo de l'appareil][7]

Références produit
------------------

```
Constructeur N° 412
DCK190.5.FDAJ.00
1500W           230V ~ 50Hz
37/12
OF N° 1208660   IP24
```

- ![photo de la plaque de référence produit][1]

---

Appareil HS
-----------

Carte de commande avec un condensateur HS

- ![photo de la carte d'origine (HS)][2]
- ![photo de la carte d'origine (HS)][3]

Référence condensateur :

```
VISHAY      F1778 MKP / SH
            1µ FX2 (K)
AC275V          40/100/56
       IEC60384-14/2
       EN 132400
---------------------------
    0422F
```

Remplacement par un modèle de composant différant, mais à capacité identique:

- ![photo du nouveau condensateur][8]
- ![photo de la carte réparée][6]
- ![photo de la carte réparée][4]
- ![photo de la carte réparée][5]

Et voilà !
----------

---

MàJ du 28 nov 2021
---------------

En janvier 2021 le condensateur a de nouveau éclaté. Le boitier semble trop exposé au flux d'air chaud. Je remplace le condensateur et protège le boitier du flux d'air chaud avec de l'aluminium autocollant.
[![photo de la carte HS][12]][11]
[![photo de la carte réparée et protégée][10]][9]


---

Merci qui ?
-----------

- [L'atelier Soudé](http://atelier-soude.fr/) (_Villeurbanne_, _Vaulx en Velin_,  _Lyon_,  _Oullins_,  _Givors_,  etc.)
- [AG Électronique](https://www.ag-electronique.fr) (_45, Cours de la Liberté Lyon 3_)

[1]: {static}/img/20191116-162716-diy-chauffage.jpg
[2]: {static}/img/20191109-161702-diy-chauffage.jpg
[3]: {static}/img/20191109-161558-diy-chauffage.jpg
[4]: {static}/img/20191116-163107-diy-chauffage.jpg
[5]: {static}/img/20191116-165130-diy-chauffage.jpg
[6]: {static}/img/20191116-163014-diy-chauffage.jpg
[7]: {static}/img/20191119-212941-diy-chauffage.jpg
[8]: {static}/img/20191120-131749-diy-chauffage.jpg
[9]: {static}/img/20210123-185712-diy-chauffage.jpg
[10]: {static}/img/20210123-185712-diy-chauffage-400p.jpg
[11]: {static}/img/20210119-091024-diy-chauffage.jpg
[12]: {static}/img/20210119-091024-diy-chauffage-400p.jpg
