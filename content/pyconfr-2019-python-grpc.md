Title: Micro-services pour une application d'analyse sémantique de textes
Date: 2019-11-03 16:00
Summary:gRPC/Python : Exemple pratique d'utilisation de micro-services pour une (mini) application d'analyse sémantique de textes (NPL)
Category: Bloc-notes
Tags: pyconfr, talk, bordeaux, dev, grpc, micro-services, npl, python
Status: published

### [gRPC/Python : Exemple pratique d'utilisation de micro-services pour une (mini) application d'analyse sémantique de textes (NPL)][1]

Par [Lionel Atty][2] − Salle [Charles Darwin][cdarwin] − Dimanche à 15 h 00

mots clés: Python gRPC micro-services NPL Spacy packaging architecture Docker

![logo PyConFr Bordeaux 2019][pyconimg]
Après une (courte) présentation du framework Google gRPC (paradigmes, protocoles, API de communications, ...), on s'intéressera à son binding spécifique Python. On présentera une utilisation (concrète) des outils du framework gRPC (Python) via le développement d'une (mini) application Python.

L'application permet d'analyser des contenus textes (texte associé à des tweets). Cette analyse (sémantique) s'appuie sur des outils (python) de NPL (Natural Processing Langage). L'étude (du développement) de cette application nous offre un cadre de (semi)production, à travers lequel on pourra aborder des considérations de production (ou mise en production) d'applications python.

On s'intéressera particulièrement à des notions d'architecture et (un peu) d'infras:

*   propositions de stratégie de packaging/structuration/versionnement de projets python avec micros-services
*   tests unitaires: comment tester des micro-services gRPC ? (framework de tests: PyTest)
*   docker: outils de dev, containeurs de production, stratégie de releases
*   CI/CD: CircleCI, TravisCI

---

Notes personnelles:

Trop technique pour moi…

[1]: https://www.pycon.fr/2019/fr/talks/conference.html#grpc%2Fpython%E2%80%AF%3A%20exemple%20pratique%20d%27utilisation%20de%20micro-services%20pour%20une%20%28mini%29%20application%20d%27analyse%20s%C3%A9mantique%20de%20textes%20%28npl%29
[2]: https://github.com/yoyonel
[cdarwin]: https://fr.wikipedia.org/wiki/Charles_Darwin
[pyconimg]: {static}/img/250-pycon-fr19.png
