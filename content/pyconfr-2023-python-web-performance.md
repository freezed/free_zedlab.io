Title: Python web performance 101
Date: 2023-02-19 12:05
Summary: Python web performance 101: uncovering the root causes
Category: Bloc-notes
Tags: live-notes, pyconfr, talk, bordeaux, python, dev
Status: published

Par **[Alex Ptakhin][author]** − Salle [Rosalind Franklin][rfranklin]


### [Python web performance 101][abstract]

[![logo PyConFr Bordeaux 2023][pyconimg]][pyconfr]

> Web engineers meet issues with performance with fast-growing or even maintaining existing products. It’s always unexpected and we have limited time for decisions. With our hero, we meet real-faced RAM, CPU and IO problems and learn troubleshooting approaches to monolithic and distributed systems.
>
> We try different existing tools from Python and the cloud ecosystem including, but not limited to: cProfile, yappi, memory-profiler and tracing.
>
> This talk will be more focused on backend parts and designed for intermediate-level web engineers, but all skill levels are welcome.

_[Support][support]_

---

Notes personnelles
==================

* …
* Temporary solution: update hardware to get time for debug
* tools
    - `time.?perf`
    - `cProfile` / [`snakeziz`](https://jiffyclub.github.io/snakeviz/)
    - /!\ mesurement change system behavior
    - `py-spy`
    - `yappi` for `asyncio`
    - `sys.getsizeof`
    - `tracemalloc`
    - `memory-profiler`: require code changes
    - `memray`: looks promising
    - Tracing: OpenTelemetry
* Things to remember
    1. getting time by adding ressources worth it
    1. Monitor application errors
    1. Measuring something can change the behaviour of the system
    1. Tuning is good, and remember, pure Python is not about the performance


[abstract]: https://www.pycon.fr/2023/fr/talks/30m.html#python-web-performance-101-unc
[author]: https://github.com/aptakhin/
[pyconfr]: https://www.pycon.fr/2023/
[pyconimg]: {static}/img/200-pycon-fr-23.png
[rfranklin]: https://fr.wikipedia.org/wiki/Rosalind_Franklin
[support]: https://github.com/aptakhin/talks/tree/main/2023-Python-Web-Performance-101
