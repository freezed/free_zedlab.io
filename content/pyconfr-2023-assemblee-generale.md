Title: Assemblée générale de l’AFPy
Date: 2023-02-19 09:22
Summary: SUMMARY
Category: Bloc-notes
Tags: live-notes, pyconfr, talk, bordeaux, python, TAG1, TAG2
Status: published

Par **[Marc Debureaux][mdebureaux]** - Salle [Alfred Wegener][awegener]


### [Assemblée générale de l’AFPy][abstract]

[![logo PyConFr Bordeaux 2023][pyconimg]][pyconfr]

> Assemblée générale de l'Association Francophone Python, organisatrice de l'événement. Vous pouvez venir pour connaître le bilan de cette année et participer aux choix des prochaines éditions. Les membres à jour de cotisation peuvent également prendre part aux votes et candidater pour faire partie du comité de direction.

---

Notes personnelles
==================

* Bilan moral
    - reprise pyconfr après 2 ans d'arrêt
    - pas de problème majeurs
    - Strasbourg n'a pas pu se concrétiser pour pyconfr 22/23 pour cause de travaux réalisé pendant la CoViD
    - Projets de partenariat avec l'[Euro Python Society](https://www.europython-society.org/): Collaboration informelle à développer
    - Réflexion autour de la convergence entre les engagements associatifs des membres
    - Bilan positifs
    - 2 abstentions
* Bilan financier
    - Comptes publics: https://git.afpy.org/afpy/gestion
    - 2022, année blanche, fond de roulement 1210€
    - Plus de serveur à payé suite au sponsoring de [Gandi](https://gandi.net)
    - Bilan PyConFr excédentaire (provisoire) ~30k€
    - PyConFr 19 ~40k€: augmentation générales des tarifs. Prestations non retenu en 2023: salle réception offerte, pas de transcription dans les vidéos
    - 2 abstentions
* Status
    - Invalidation de la préfecture: retour sur le contexte
    - modif: art. 1 (domiciliation), art.9  (nombres de membres et durée des mandats)
    - 2 abstentions
* Renouvelement du comité directeur
    - démissionnaire de fait: Marc, Lucie
    - candidats: Nicolas Ledez, Lucie Anglade, Marc Debureaux
        * Présidente: Lucie Anglade

[abstract]: https://www.pycon.fr/2023/fr/talks/plenary.html#assemblee-generale-de-lafpy
[awegener]: https://fr.wikipedia.org/wiki/Alfred_Wegener
[mdebureaux]: https://fr.linkedin.com/in/mdebnet
[pyconfr]: https://www.pycon.fr/2023/
[pyconimg]: {static}/img/200-pycon-fr-23.png
