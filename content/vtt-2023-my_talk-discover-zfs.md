Title: Découvrez ZFS : un stockage fiable, puissant et accessible.
Date: 2023-02-02 
Category: Conférences
Summary: Découvrez ZFS: un stockage fiable, puissant et accessible.
Status: Published
Tags: vtt, talk, paris, storage, zfs, admin, cli, backup

Le 2 février 2023 j'ai partagé cette présentation dans le cadre du [VeryTechTrip OVH][vtt] à Paris.

[![logo VTT 2023]({static}/img/vtt-2023.png)][captation]

J'y ai présenté [ZFS][openzfs] un système de fichier qui est aussi un gestionnaire de volumes.

> `ZFS` est un outil passionnant qui va au-delà d'un système de fichier. Cette présentation se veut techniquement accessible et vise à partager ma découverte de cet outil pour (peut-être) vous donner envie de l'essayer.

> Né au début des années 2000 au sein de _Sun Microsytems_, ZFS est aujourd'hui développé au travers du projet [openZFS][openzfs] pour les noyaux _Linux_ et _freeBSD_.

![openzfs logo]({static}/img/openzfs.png)

Supports de présentation : [🇫🇷]({static}/pdf/20230203-vtt-openzfs-fr.pdf) / [🇬🇧]({static}/pdf/20230203-vtt-openzfs-en.pdf) /  [📝 `ftalk/openzfs`](https://gitlab.com/ftalk/openzfs/-/tree/vtt/)

[Lien vers la captation vidéo][captation]


[vtt]: https://verytechtrip.ovhcloud.com/fr/
[openzfs]: https://openzfs.org
[captation]: https://player.vimeo.com/video/804391464 
