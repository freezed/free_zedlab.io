Title: Ajouter un VPN dans Debian/buster MATE
Date: 2019-04-12 08:52
Category: Bloc-notes
Status: published
Summary: Ajout d'une connexion VPN dans le network-manager-gnome pour l'environnement de bureau MATE
Tags: vpn, debian, mate, gnome, admin

Context
=======

```
~ lsb_release -a;mate-about --version;uname -r
Description:	Debian GNU/Linux buster/sid
Release:    	testing
Codename:   	buster
MATE Desktop Environment 1.20.4
4.19.0-4-amd64
```

Installation
============

Dépendences
-----------

```
~ sudo apt install openvpn network-manager-openvpn network-manager-openvpn-gnome openvpn-systemd-resolved
```

Redémarrage
-----------

```
~ sudo service network-manager restart
```

Config
------

Automatique via un fichier `*.ovpn` :

```
~ sudo nmcli connection import type openvpn file /chemin/du/fichier_de_conf.ovpn
```

Ajout de l'utilisateur à la nouvelle connection créé depuis le menu _réseau_ du _centre de contrôle_ :

```
~ mate-control-center
```

---

Source : [_«How to import a OpenVPN .ovpn file with Network Manager or Command Line in Linux»_](https://www.cyberciti.biz/faq/linux-import-openvpn-ovpn-file-with-networkmanager-commandline/)
