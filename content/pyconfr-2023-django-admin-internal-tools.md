Title: Django Admin comme framework pour développer des outils internes
Date: 2023-02-18 10:05
Summary: Comment remplacer Excel©® par Django Admin
Category: Bloc-notes
Tags: live-notes, pyconfr, talk, bordeaux, python, django, business, dev, logiciel-libre,  web
Status: published

Par **[Romain Clement][author]** - Salle [Charles Darwin][cdarwin]


### [Django Admin comme framework pour développer des outils internes][abstract]

[![logo PyConFr Bordeaux 2023][pyconimg]][pyconfr]

> Si vous développez des applications avec le framework Django, vous connaissez sûrement Django Admin pour introspecter votre base de données et effectuer quelques opérations de maintenance. Mais savez-vous qu'il est possible de développer des applications complètes grâce à Django Admin ?
>
> Django Admin possède des secrets bien gardés mais une fois découverts, beaucoup de possibilités s'offrent à nous. De la gestion de permissions avancée à l'ajout de pages et formulaires personnalisés en passant par l'intégration d'automatisations, développer des outils métiers devient un jeu d'enfant.
>
> Dans cette présentation, je vous partagerez mon retour d'expérience sur l'utilisation de Django Admin comme framework à part entière dans le cadre de développement d'applications internes centrées sur les données.
>
> Les applications "no-code" n'ont qu'à bien se tenir !

_[Support][support]_

---

Notes personnelles
==================

* Comment remplacer Excel©® par Django Admin
    - prolifération de tableaux dans tous les services
    - objectifs:
        * centraliser rles données
        * uniformiser
        * calcul
        * contrôle d'accès
* les choix alternatifs
    - sur mesure
    - low/nocode
    - django-admin
* Avec un modèle de donnée basique, outilages fournis
    - accès restreints par groupe
    - ajout de vues
* Trucs et astuces
    - utiliser `fieldset`
    - vue agrégée: non managé par django (`managed = False`) et vue en pure SQL
    - contrôle d'accès
    - route configurable
* _partit avant la fin…_

❓Quel _pour/contre_ : django _client_ vs. django _admin_


[abstract]: https://www.pycon.fr/2023/fr/talks/30m.html#django-admin-comme-framework-p
[author]: https://romain-clement.net/
[cdarwin]: https://fr.wikipedia.org/wiki/Charles_Darwin
[pyconfr]: https://www.pycon.fr/2023/
[pyconimg]: {static}/img/200-pycon-fr-23.png
[support]: https://
