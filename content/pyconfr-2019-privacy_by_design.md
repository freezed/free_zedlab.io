Title: Privacy by design
Date: 2019-11-02 15:00
Summary: « Privacy by design » : protéger les données personnelles, 1 million de lignes de code à la fois
Category: Bloc-notes
Tags: pyconfr, talk, bordeaux, django, web, privacy, dev, logiciel libre
Status: published

### [« Privacy by design » : protéger les données personnelles, 1 million de lignes de code à la fois][1]

Par [François Séguin][4] − [Salle Rosalind][rfranklin] − Samedi à 14 h 30

![logo PyConFr Bordeaux 2019][pyconimg]
Câbles diplomatiques américains, emails de Sony Pictures, photos de prototype d'iPhone : on sait que ces informations sont confidentielles, et elles ont pourtant été victimes de fuites. Les informations personnelles de nos utilisateurs sont tout aussi confidentielles, et nous devons les protéger. Les logs et interfaces d'une application web exposent souvent des données personnelles: comment les expurger et/ou restreindre leur accès?

Je partagerai mes travaux sur la protection des données personnelles dans un écosystème mature de plusieurs millions de lignes de code, et du paquet open-source pour Django que nous avons créé.

---

Notes personnelles:

_Caviarder_ remontant au _[Tsar Nicolas 1er][2]_

Création d'un type héritant de `str()` permettant de pseudonymiser la valeur.

Usage de _middleware_ et autre dispositions de Django pour la mise en œuvre.

Code _en cours_ de libération à venir dans le _package_ [django-caviar][3]

[1]: https://www.pycon.fr/2019/fr/talks/conference.html#%C2%AB%E2%80%AFprivacy%20by%20design%E2%80%AF%C2%BB%E2%80%AF%3A%20prot%C3%A9ger%20les%20donn%C3%A9es%20personnelles%2C%201%C2%A0million%20de%20lignes%20de%20code%20%C3%A0%20la%20fois
[2]: https://fr.wiktionary.org/wiki/caviarder
[3]: https://pypi.org/project/django-caviar
[4]: https://github.com/francois-seguin
[pyconimg]: {static}/img/250-pycon-fr19.png
[rfranklin]: https://fr.wikipedia.org/wiki/Rosalind_Franklin
