Title: Un jeu de labyrinthe avec Pygame
Date: 2018-04-24 11:11
Category: Réalisations
Status: published
Slug: jeu-labyrinthe-pygame
Summary: Aidez MacGyver à s'échapper du labyrinthe…
Tags: pygame, python, git


## Description

MacGyver a été enfermé dans un labyrinthe. La sortie est surveillée par un garde et pour le distraire, il faut combiner les éléments suivants dispersés dans le labyrinthe :

* une aiguille
* un tube
* éther

Avec ces éléments MacGyver fabriquera une seringue et pourra endormir le garde.

[![Copie d'écran][ocp3img]][ocp3]

## Librairie utilisée

* [Pygame 1.9](https://pygame.org)

---

Voir le code hébergé sur [Github][ocp3]


[ocp3]: https://github.com/freezed/ocp3/
[ocp3img]: {static}/img/ocp3.jpg
