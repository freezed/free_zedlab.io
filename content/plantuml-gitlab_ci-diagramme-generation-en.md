Title: Génération de diagrammes PlantUML avec Gitlab-CI
Date: 2019-07-13 10:00
Summary: Générer des diagrammes PlantUML à l'aide de l'intégration continue & l'hébergement statique de Gitlab
Category: Bloc-notes
Tags: uml, gitlab, gitlab-pages, statique, ci, dry, devops, web, numerisation
Status: published
Lang: en
Slug: plantuml-gitlab_ci-diagramme-generation


_The easy way to keep diagrams up-to-date with the code_

What ?
------

Diagrams are :

* **100%** text described
* built with _gitlab-ci_
* deployed on _gitlab-pages_

So easy to maintain that your diagrams could be **still up-to-date** even **10 months later**!!!

How ?
-----

Just push [diagram sources][src] and [.gitlab-ci.yml][yml] will build & deploy it on _pages_ :

![Physical data model with shared files][pdm-shared].

[D.R.Y.][dry] !
---------------

Using _!include_ or _!includeurl_ PlantUML's [preprocessor][preprocessor] directives allows you to share common sources between diagrams.

In this repo, _shared files_ diagrams [share same data][part] :

- associations
- attributes
- classes
- keys
- legend
- relations

Hence, if you add something in your shared sources, all diagrams will get it !

[Talk is cheap. Show me the code.][show]
----------------------------------------

| _Diagram type_              | Single file                                             | Shared files |
|:---:                        |:---:                                                    |:---:|
|_Functional domain_          | -                                                       | ([diagram][fm-shared]) - [[source][fm-shared-src]] - [common parts][part]   |
|_Functional domain detailed_ | ([diagram][fmwa-single]) - [[source][fmwa-single-src]]  | ([diagram][fmwa-shared]) - [[source][fmwa-shared-src]] - [common parts][part]   |
|_Physical data model_        | ([diagram][pdm-single]) - [[source][pdm-single-src]]    | ([diagram][pdm-shared]) - [[source][pdm-shared-src]] - [common parts][part]   |


[dry]: https://en.wikipedia.org/wiki/Don't_repeat_yourself "Don't repeat yourself"
[fm-shared]: https://free_zed.gitlab.io/mypumlt/diagrams/fm-shared_files.png "Functional model with shared files"
[fm-shared-src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/fm-shared_files.puml "Diagram sources"
[fmwa-single]: https://free_zed.gitlab.io/mypumlt/diagrams/fmwa-single_file.png "Functional model (with attributes) single file"
[fmwa-single-src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/fmwa-single_file.puml "Diagram sources"
[fmwa-shared]: https://free_zed.gitlab.io/mypumlt/diagrams/fmwa-shared_files.png "Functional model (with attributes) with shared files"
[fmwa-shared-src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/fmwa-shared_files.puml "Diagram sources"
[part]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/part "Diagram parts sources"
[pdm-shared]: https://free_zed.gitlab.io/mypumlt/diagrams/pdm-shared_files.png "Physical data model with shared files"
[pdm-shared-src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/pdm-shared_files.puml "Diagram sources"
[pdm-single]: https://free_zed.gitlab.io/mypumlt/diagrams/pdm-single_file.png "Physical data model single file"
[pdm-single-src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/pdm-single_file.puml "Diagram sources"
[plantuml]: http://plantuml.com "Open-source tool to draw UML (or not) diagrams"
[preprocessor]: http://plantuml.com/en/preprocessing#9 "Preprocessing"
[show]: https://lkml.org/lkml/2000/8/25/132
[src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/ "Diagram sources"
[yml]: https://gitlab.com/free_zed/mypumlt/blob/master/.gitlab-ci.yml
