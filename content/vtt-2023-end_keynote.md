Category: Bloc-notes
Date: 2023-02-02 18:15
Status: published
Summary: 45 min d'intéractions tech, voilà le challenge qu'ils vont relever !
Tags: vtt, ovh, paris, keynote, business
Title: Keynote de cloture

Par [Octave Klaba][oles] & [Thierry Souche][tsouche] - [**KEYNOTE** _Very Tech Trip_][vtt]

### Keynote de cloture

> 45 min d'intéractions tech, voilà le challenge qu'ils vont relever !

---

Notes personnelles
==================

1. Les termes bio climat et écologie sont absents du programme de cette journée, alors que ce sont des enjeux colossaux pour l'humanité. L'industrie de l'informatique se met-elle la tête dans le sable?
    * Octave: _Tout nos produits sont bio_. (…)
1. Demain: plus d'informaticien, ou moins d'informaticien? Et quel informaticien?
    * Thierry: Informaticien, ça ne veut plus dire grand choses…
1. Pensez vous vraiment que l'IPv6 sera la norme en entreprise dans le futur? Où en est le déploiement de soluution IPv6 failover chez OVHcloud?
    * Octave: _Y'a des claques qui se perdent autour du déploiement d'IPv6…_
1. Soutenez vous toujours le monde de l'OpenSource? Quels sont vos engagements?
    * Thierry: Oui…
1. OVHcloud a fait plusieurs aquisitions ces dernières années: OpenIO, ForePaas… Est-ce le signe que le métier d'OVHcloud évolue, de la gestion d'infra / fourniture de IaaS vers la conception de logiciels pour consommer…
    * Octave: Oui…
1. Comment gérez vous les cycles de vie des machines? Que sont devenues les offres Kimsufi et SoYouStart qui permettaient de donner une seconde vie aux serveurs OVHcloud?
    * Thierry: Récap de waterfalling et explications des gamme Rise / eco
1. Comment OVHcloud dont le métier est plutôt la centralisation et la mutualisation des infrastructures, peut-il nous accompagner sur le Edge Computing?
    * Thierry & Octave …
1. _dernière question manquante_


[oles]: https://www.linkedin.com/in/octave-klaba-3a0b3632
[tsouche]: https://www.linkedin.com/in/thierrysouche
[vtt]: https://verytechtrip.ovhcloud.com/fr/very-tech-trip/sessions/
