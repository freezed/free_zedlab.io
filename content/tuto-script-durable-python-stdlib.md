Title: Tuto  : script durable en Python
Date: 2021-04-23 00:44
Modified: 2021-12-02 00:19
Summary: Python est livré avec les piles, profitons-en.
Category: Réalisations
Tags: forga, cli, python, git, dry, gitlab, agile, tuto,  méthode, collectif, dev
Status: published
Slug: tuto-script-durable-python-stdlib


💡 C'est quoi l'idée ?
=====================

Un guide de construction d'un script python CLI robuste et maintenable même plusieurs mois après l'avoir laissé de côté! Python est [_livré avec les piles_][py-batteries], profitons-en et utilisons pour ça :

- les tests : [`doctest`][doctest]
- les logs : [`logging`][logging]
- les arguments de la CLI : [`argparse`][argparse]

C'est le 1er tuto dans la liste [_«Parcours de tutorat»_][forga-emb], _Un script durable en Python_.

Plus de détails dans ma proposition de réalisation :

- [le ticket associé][pysdur-issue]
- [les commits][pysdur-commit]


🌐 Quel est le contexte ?
========================

Le groupe [`forga`][forga] a pour objectif de proposer une organisation facilitant :

- ♻️ la réutilisation de code
- 📝 la documentation (de code ou d'autre chose)
- 🤝 la collaboration

Ce groupe à été brièvement abordé dans une [présentation][meetup-forga] pour le groupe _GitLab FR_.

🤝 Merci qui ?
==============

_Vincent Bernat_ pour son billet de blog dont je me suis complètement inspiré :

* [🇫🇷 _Écrire un script Python durable_](https://vincent.bernat.ch/fr/blog/2019-script-python-durable)
* [🇬🇧 _Writing sustainable Python scripts_](https://vincent.bernat.ch/en/blog/2019-sustainable-python-script)



[meetup-forga]: {filename}/afpy-lyon-2019-11-devel-orga.md
[forga-emb]: https://gitlab.com/forga/process/fr/embarquement
[pysdur-issue]: https://gitlab.com/forga/process/fr/embarquement/-/issues/6
[pysdur-commit]: https://gitlab.com/free_zed/mypsb/-/commits/tuto-pysdur
[forga]: https://gitlab.com/forga/
[doctest]: https://docs.python.org/3/library/doctest.html
[logging]: https://docs.python.org/3/library/logging.html
[argparse]: https://docs.python.org/3/library/argparse.html
[py-batteries]: https://docs.python.org/3/tutorial/stdlib.html#batteries-included
