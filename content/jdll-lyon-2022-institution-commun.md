Title: Quand une institution publique s'intéresse à un commun
Date: 2022-04-02 11:00
Summary: Le ministère de la Culture a lancé un dictionnaire collaboratif numérique à partir du Wiktionnaire.
Category: Bloc-notes
Tags: talk, jdll, lyon, collectif, citoyen
Status: Published
Lang: fr

### [Quand une institution publique s'intéresse à un commun, le cas du Dictionnaire des francophones](https://pretalx.jdll.org/jdll2022/talk/ACWCTJ/)

par [Noé Gasparini](https://www.univ-lyon3.fr/gasparini-noe) - 2022-04-02 11:00–11:55 Atelier du mouvement

![logo JDLL 2022]({static}/img/jdll-2022.jpg)

Le ministère de la Culture a lancé un dictionnaire collaboratif numérique à partir du Wiktionnaire.

En 2018, le président de la République française initie un nouveau plan pour la langue française incluant un nouvel objet numérique, le Dictionnaire des francophones. Celui-ci s'adresse à l'ensemble des francophones et propose d'adapter l'ordre de son contenu selon les lieux d'usage du français. Cette nouvelle base de connaissances libre intègre plusieurs ressources existantes, dont le Wiktionnaire, un dictionnaire collaboratif libre, projet frère de Wikipédia. Un projet libre qui fonctionnait déjà bien et se trouve ainsi ré-affiché au sein d'une nouvelle interface proposée par une institution publique.

Un commun propulsé par une institution publique ? Le Wiktionnaire a-t-il ainsi gagné ses lettres de noblesse ou vient-il de se faire forker ? Quels sont les valeurs des deux objets numériques ? Sont-ils concurrents ou plutôt copains ?

Venez en apprendre un peu plus sur ces différents projets, par les personnes impliquées, et poser vos questions sur cette initiative originale de création d'un nouveau commun à partir d'un commun existant.

_[MàJ du 3/10/22]:_ [Lien vers la captation vidéo][captation]

---

Notes personnelles:

N/A

[captation]: https://www.videos-libr.es/w/oyzhSH1TodQDKyVBJY66Xb
