Title: Un IDE pour les gouverner tous
Date: 2022-11-16 19:00
Lang: fr
Status: published
Summary:
Category: Bloc-notes
Tags: afpy, talk, lyon, dev, cli, python, ovh, git, live-coding,

Par [Ioannis][author], organisé par [CourtBouillon][cbouillon], [Stella][stella] et l'[AFPy][afpy]. (via [Meetup][meetup]).

_**Support**: sur [`voltux/nvim_ide_presentation`][support]_

### Un IDE pour les gouverner tous

> Besoin de développer un projet avec toutes les fonctionnalités d'un IDE:
>
> * En éditant du code plus vite que la lumière
> * Sans environnement graphique (VM, container, ssh...)
> * Sans droits admin
> * Avec des ressources très limitées
> * Sur des environnements toujours différents
> * Sur un outil configurable de A à Z
> * Sur un outil open source
> * Sur un outil qui gère aussi vos slides de présentation et votre Todo liste
> * De façon à pouvoir se la jouer auprès de ses collègues
>
> Si la réponse est oui à au moins une de ces questions, venez découvrir Neovim et son écosystème de plugins en tant que IDE Python.
>
> On va monter un docker debian, créer un utilisateur sans droits admin, installer tous les outils dont on a besoin et travailler sur un petit code sympathique pour remplir des sudoku
>
> On va bien sûr utiliser Neovim aussi pour afficher les slides!
>
> Retrouvez les ressources de cette présentation sur [GitHub][support] !

---

Notes personnelles
==================

* Ioannis
    - Ingénieur mathématique, professeur, traducteur et journaliste
    - déteste les sudokus
* Historique
    1. `ed` -> `em` -> `ex` -> `vi` -> `vim` (Bram Moolenaar - 1991)
    1. `neovim` (community -> 2015): modernisation et intégration de `lua`
* Démarrer
    - Modale:
        * Normal
        * Insertion
        * Visuel
        * Commande
    - `vimtutor`
    - https://vim-adventures.com
    - `:h …` page d'aide'
* Construire sa configuration de zéro pour ne pas se perdre et répondre à ces besoin
* Fonctionnalité très avancée: 
    - macros
    - explorateur de fichier
    - [Language Server Protocol](https://en.wikipedia.org/wiki/Language_Server_Protocol)
* Liste de course
    - ligne "centrée"
    - navigateur fichier
    - terminal (fenêtre ou pas ?)
    - fenetrage
    - snippet
    - LSP
    - undo arborescent
    - surround


[afpy]: https://www.afpy.org/
[author]: https://github.com/voltux/
[cbouillon]: https://www.courtbouillon.org/
[meetup]: https://www.meetup.com/fr-FR/python-afpy-lyon/events/289353961/
[stella]: https://stella.coop/
[support]: https://github.com/voltux/nvim_ide_presentation
