Title: Rust Lyon Meetup #4
Date: 2023-05-24 19:00
Summary: Rencontre autour du langage de programmation Rust
Category: Bloc-notes
Tags: talk, lyon, dev, rust
Status: published

Par [Maxime Mikotajewski][author-a] & [Nolwenn Doucet][author-b], organisé par [Rust Lyon Meetup][org]. (via [Meetup][meetup]).


### Rust Lyon Meetup #4

> Rencontre autour du langage de programmation Rust, de ses usages, de son évolution, de sa pratique, entre personnes l'utilisant ou curieuses de le découvrir.
>
> Au programme de la soirée : quelques présentations (voir ci-dessous), puis ensuite discussions en plus petits groupes et réseautage.


---

Notes personnelles
==================

Mon apprentissage de Rust
-------------------------

- Base
    * Rust book
    * Rust by examples
    * Rustlings
    * exercism
    * lifetimes katas
- au dela des bases
    * clippy
    * livres:
        - Rust for Rustaceans (Jon Gj???)
        - Zero to production in Rust, Luca Palmieri
        - Rust in action, Tim McNamara: plutôt orienté systèmes)
- Ensuite…
    * This week in Rust (newsletters)
    * Rust magazine (magazine en ligne)
    * Awesome Rust mentors (annuaire)
- Conclusion
    * qualité de l'ecosysteme
    * petit manque de contenu intermédiaire
    * qualité de la communauté

Q/A:

* Programming Rust (O'reilly)
* Crust of Rust (youtube - Jon Gj???)


La valeur de _Value Objects_
----------------------------

* 😅



[author-a]: https://fr.linkedin.com/in/maxime-mikotajewski-86112a10b
[author-b]: https://fr.linkedin.com/in/nolwenndoucet
[org]: https://www.meetup.com/fr-FR/rust-lyon/
[meetup]: https://www.meetup.com/fr-FR/rust-lyon/events/293322211
