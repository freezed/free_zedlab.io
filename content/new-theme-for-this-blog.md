Title: Un nouveau thème pour ce blog
Date: 2020-09-07 01:26
Summary: Il était temps de passer à un thème responsive
Category: Bloc-notes
Tags: pelican, web, gitlab-pages, ci, statique
Status: Published

Enfin un thème _responsive_, basé sur le thème `uberspot` de [Achilleas Pipinellis](https://gitlab.com/axil) publié sous licence [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr).

Mes adaptations :

- lien vers le commit de _build_ du site;
- suppression de _templates_ (`disqus`, `js_https`, etc.);
- suppression des _pluggins_;
- personnalisation de quelques blocs;
- suppression de la **page d'accueil** de l'ancien thème et intégration de son contenu dans la **page contact**;

Plus d'info sur ce thème sur [le post originel de son auteur](https://axilleas.me/en/blog/2014/pelican-new-theme-redesign/).

Merci à lui.
