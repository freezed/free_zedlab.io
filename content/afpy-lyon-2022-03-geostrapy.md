Title: Histoires d'un sportif perfectionniste sous pression [AFPy]
Category: Conférences
Date: 2022-03-31 19:00
Lang: fr
Status: published
Summary: Garder le contrôle sur les données que je partage avec des services en ligne.
Tags: afpy, talk, lyon, dev, cli, python, privacy


### Garder le contrôle sur les données que je partage avec des services en ligne.

Cette présentation c’est déroulée dans le cadre des [rencontres lyonnaises et mensuelles de l'AFPy][afpy] organisées par [CourtBouillon][cbouillon], [Stella][stella] et l'[AFPy][afpy]. (via [Meetup][meetup])

![logo AFPy Lyon][afpyimg]

> _Strava_, _runkeeper_, _runtastic_, … sont des réseaux sociaux pour les activités sportives basé sur l'usage de grandes quantité de données géolocalisés pour fournir en retour divers services.

> Le fonctionnement par défaut ne permet pas facilement de limiter les données transmises et le coût d'usage en données personnel devient très vite élevé si on n'utilise pas la totalité des services proposé.

> J'ai souhaité construire un outil qui permettrait de choisir les données que je transmet, me permettant de bénéficier des quelques services de mon choix en limitant les données utilisé par les services qui ne m'intéressent pas.

> C'est un projet expérimental et ouvert qui permet de mettre en lumière l’économie de la donnée des grands services en ligne.

- Support de présentation : [📦️ archive][support] / [📝 `fcode/geostrapy`][support-froggit]
- Dépôt de code: [`fcode/geostrapy`][repo]


[afpy]: https://www.afpy.org/
[afpyimg]: {static}/img/afpylyon-200.png
[cbouillon]: https://www.courtbouillon.org/
[meetup]: https://www.meetup.com/fr-FR/Python-AFPY-Lyon/events/285563922/
[meetup]: https://www.meetup.com/Python-AFPY-Lyon/events/282750718/
[repo]: https://lab.frogg.it/fcode/geostrapy
[stella]: https://stella.coop/
[support-froggit]: https://lab.frogg.it/fcode/geostrapy/-/blob/12-afpy-talk/docs/20220331-afpy-geostrapy.pdf
[support]: {static}/pdf/20220331-afpy-geostrapy.pdf
