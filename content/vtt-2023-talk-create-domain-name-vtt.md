Title: Et si nous créions le .vtt ?: Et si nous créions le .vtt ?
Category: Bloc-notes
Date: 2023-02-02 14:15
Status: published
Summary: De l'idée de création jusqu'à la mise en vente de l'extension .vtt, nous découvrirons ensemble le fonctionnement des DNS et des noms de domaine.
Tags: vtt, ovh, talk, paris, dns, domain


Par [Eric Vergne][author-eric] & [Benoît Masson][author-benoit] - [**TALK** _Very Tech Trip_][vtt]

_[**Support de présentation**][support]_

### Et si nous créions le `.vtt` ?

> Les noms de domaine et les DNS font partit des services primaires d'internet. De l'idée de création jusqu'à la mise en vente de l'extension .vtt, nous découvrirons ensemble le fonctionnement des DNS et des noms de domaine. Les termes Registry, Registrar, Icann, DNS racine/autoritaire et Resolver, Whois, EPP et gouvernance d'internet ne vous seront alors plus inconnus. Vous comprendrez également pourquoi ce milieu est si sensible au changement.


---

Notes personnelles
==================

* Pourquoi?
    - ne plus partatger de fichier `/etc/hosts`
* Comment?
    - Créer des autorité de délégations pour les extentions
    - `www.verytechtrip.com`
        * Résolveur: FAI, Cloudflare, Gxxgle, …
        * `computer` -> `resolver` -> who manage .com?
        * `computer` -> `resolver` -> who manage .verytechtrip.com?
        * `computer` -> `resolver` -> answers `computer
        * (again): `computer` -> `resolver` -> answers `computer` **from cache**
        * 13 servers DNS root (IP 32bits)
* Acteurs
    - ICANN / registrar / registry
* Comment devenir _registry_?
    - beaucoup de demande?
    - pourquoi faire?
    - créer le dossier de demande
        * prix 2012: 185000$ / demain: 250000$
        * plusieurs étapes et délai long et prolongeables
        * Objections: `.wine`, `.amazon`,
        * vente aux enchères:
            1. `.tech` - 5 millions euro (dot tech, then radix)
            1. `.blog` - 15M (automattic <- wordpress)
            1. `.app`  - 25M (google)
            1. `.shop` - 41.5M (GMO registry)
            1. `.web`  - 135M (Nu Dot Co <- verisign) = 11 ans plus tard toujours pas de résolution du conflit
* Deploy an infrastructure


[author-benoit]: https://www.linkedin.com/in/benoitmasson
[author-eric]: https://www.linkedin.com/in/ericvvergne
[support]: https://ovh.to/tFHgKiH
[vtt]: https://verytechtrip.ovhcloud.com/fr/very-tech-trip/sessions/
