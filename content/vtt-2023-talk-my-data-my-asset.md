Title: Ma donnée est mon asset !
Date: 2023-02-02 17:35
Summary: Partageons les bonnes pratiques à suivre pour protéger ses jeux de données
Category: Bloc-notes
Tags: vtt, ovh, talk, paris, backup, object, storage, security, process, cli
Status: published

Par [Charlotte Letamendia][author] - [**TALK** _Very Tech Trip_][vtt]


### Ma donnée est mon asset ! Partageons les bonnes pratiques à suivre pour protéger ses jeux de données

> Dans ce talk, nous parlons Object Storage et API S3 pour vous apprendre à suivre les bonnes pratiques de sécurité et garantir l'immutabilités de vos back-ups. Face à l'augmentation du risque de hacking, sous différentes formes, nous souhaitons rappeler quelques basiques de la sécurité de données adaptés à l'object storage. Vous apprendrez à utiliser les API S3 et mettre en place l'encryption S3-SSE, les user policy, le versioning, l'object lock, dans l'objectif de protéger la donnée et ainsi renforcer la résilience de vos infrastructures.

---

Notes personnelles
==================

* Donnée double tous les 18 mois
* Cas lors de l'usage avec stocage S3
* Risques
    - humain: suppression, écrasement
    - pannes: matériel, datacentre, origine géograpique
    - attack: malware, ransomware, virus, sabotage, DDOS, etc…
* Règles et options `s3`
    1. versionning
    1. immutabilité
    1. replication (multi région) 3 copies / 2 media / 1 offsite
    1. chiffrer
    1. ajuster les droits d'accès au minimum
        - user et bucket
        - revue régulière
* example, rapport de risque météo pour aviation
    - fichiers grib émit toutes les 6h (pas de diff)
    - => Demo


[author]: https://www.linkedin.com/in/charlotte-letamendia-434a162/
[vtt]: https://verytechtrip.ovhcloud.com/fr/very-tech-trip/sessions/
